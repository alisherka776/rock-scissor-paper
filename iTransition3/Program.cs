﻿using ConsoleTables;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        if (args.Length < 3 || args.Length % 2 == 0 || args.Length != args.Distinct().Count())
        {
            Console.WriteLine("Invalid input. Please provide an odd number of unique strings as command line arguments.");
            return;
        }

        Game game = new Game(args);
        string key = GenerateKey();
        string computerMove = game.GenerateComputerMove();
        string hmac = CalculateHMAC(key, computerMove);

        Console.WriteLine($"HMAC: {hmac}");

        ShowMenu(args);

        int userMove;
        while (true)
        {
            Console.Write("Enter your move: ");
            string userInput = Console.ReadLine();
            if (userInput == "?")
            {
                ShowHelpTable(args);
                continue;
            }

            if (!int.TryParse(userInput, out userMove) || userMove < 0 || userMove > args.Length)
            {
                Console.WriteLine("Invalid move. Please enter a number corresponding to your move or '?' for help.");
                continue;
            }

            if (userMove == 0)
                return;

            string result = game.GetResult(args[userMove - 1], computerMove);
            Console.WriteLine($"Your move: {args[userMove - 1]}");
            Console.WriteLine($"Computer's move: {computerMove}");
            Console.WriteLine($"Result: {result}");
            Console.WriteLine($"HMAC Key: {key}");
            return;
        }
    }

    static string GenerateKey()
    {
        using (var rng = new RNGCryptoServiceProvider())
        {
            byte[] keyBytes = new byte[32];
            rng.GetBytes(keyBytes);
            return BitConverter.ToString(keyBytes).Replace("-", "").ToLower();
        }
    }

    static string CalculateHMAC(string key, string message)
    {
        byte[] keyBytes = Encoding.UTF8.GetBytes(key);
        byte[] messageBytes = Encoding.UTF8.GetBytes(message);

        using (var hmac = new HMACSHA256(keyBytes))
        {
            byte[] hashBytes = hmac.ComputeHash(messageBytes);
            return BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
        }
    }

    static void ShowMenu(string[] moves)
    {
        Console.WriteLine("Available moves:");
        for (int i = 0; i < moves.Length; i++)
        {
            Console.WriteLine($"{i + 1} - {moves[i]}");
        }
        Console.WriteLine("0 - Exit");
        Console.WriteLine("? - help");
    }

    static void ShowHelpTable(string[] moves)
    {
        var table = new ConsoleTable();

        var headerRow = new List<string> { "v PC\\User >" };
        headerRow.AddRange(moves);
        table.AddColumn(headerRow.ToArray());

        for (int i = 0; i < moves.Length; i++)
        {
            var row = new List<string> { moves[i] };
            for (int j = 0; j < moves.Length; j++)
            {
                row.Add(GetResult(moves[i], moves[j], moves));
            }
            table.AddRow(row.ToArray());
        }

        table.Write(Format.Alternative);
    }

    static string GetResult(string move1, string move2, string[] moves)
    {
        int movesCount = moves.Length;
        int move1Index = Array.IndexOf(moves, move1);
        int move2Index = Array.IndexOf(moves, move2);

        int halfMovesCount = movesCount / 2;

        int winningMove1Index = (move1Index + halfMovesCount) % movesCount;

        if (move1Index == move2Index)
        {
            return "Draw";
        }
        else if (move2Index == winningMove1Index || (move1Index + 1) % movesCount == move2Index)
        {
            return "Win";
        }
        else
        {
            return "Lose";
        }
    }

}

class Game
{
    private Dictionary<string, List<string>> winningMoves;
    public int ComputerMoveIndex { get; private set; }

    public Game(string[] moves)
    {
        winningMoves = new Dictionary<string, List<string>>();
        for (int i = 0; i < moves.Length; i++)
        {
            winningMoves[moves[i]] = new List<string>();
            for (int j = 1; j <= moves.Length / 2; j++)
            {
                winningMoves[moves[i]].Add(moves[(i + j) % moves.Length]);
            }
        }
    }

    public string GenerateComputerMove()
    {
        using (var rng = new RNGCryptoServiceProvider())
        {
            byte[] bytes = new byte[1];
            rng.GetBytes(bytes);
            ComputerMoveIndex = bytes[0] % winningMoves.Count;
            return winningMoves.Keys.ToArray()[ComputerMoveIndex];
        }
    }

    public string GetResult(string userMove, string computerMove)
    {
        if (winningMoves[computerMove].Contains(userMove))
            return "You lose!";
        else if (winningMoves[userMove].Contains(computerMove))
            return "You win!";
        else
            return "It's a draw!";
    }
}
